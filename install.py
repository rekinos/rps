#!/usr/bin/env python3

import argparse
import networkx as nx
import os
import pathlib
import sqlite3
import subprocess
import sys
import urllib.request

from rkbuild import makepkg_from_archive

PACKAGE_SOURCE = "http://localhost:8000"

parser = argparse.ArgumentParser()
parser.add_argument("package")
parser.add_argument("--sysroot", type=pathlib.Path, default=pathlib.Path("/"))

args = parser.parse_args()

root_target = args.package
sysroot = args.sysroot

DB_PATH = "/tmp/test.db"
PACKAGE_CACHE_PATH = sysroot / "var/cache/rps/pkg"

PACKAGE_BUILD_TMP = sysroot / "var/tmp/rps/build"
PACKAGE_BUILD_DEST = sysroot / "var/cache/rps/pkg/built"

PACMAN_STATE_DIR = sysroot / "var/lib/pacman"

PACMAN_STATE_DIR.mkdir(parents=True, exist_ok=True)

pacman_args = []
if sysroot != "/":
    pacman_args.append("--root")
    pacman_args.append(sysroot)

if not os.path.exists(DB_PATH):
    sys.exit("Error: Database not found")

def confirm(message):
    result = input(message + " [Y/n]: ")
    if result == "" or result.lower() == "y":
        return True
    elif result == "n":
        return False
    else:
        return confirm(message)

def retrieve_package(package):
    PACKAGE_CACHE_PATH.mkdir(parents=True, exist_ok=True)
    path = PACKAGE_CACHE_PATH / (pathlib.Path(package).name)
    if path.exists():
        return path

    print("Downloading %s" % (package,))
    url = PACKAGE_SOURCE + "/" + package

    urllib.request.urlretrieve(url, path)
    return path

def query_needed_deps(names):
    proc = subprocess.run(["pacman", *pacman_args, "-T", "--", *names], capture_output=True)
    proc.check_returncode()

    return [line for line in proc.stdout.split(b"\n") if len(line) > 0]

conn = sqlite3.connect(DB_PATH)
try:
    c = conn.cursor()

    c.execute("SELECT version FROM meta")

    metarow = c.fetchone()

    VERSION = 2
    if metarow[0] != VERSION:
        raise RuntimeError("Unsupported DB version %s, expected %s" % (metarow[0], VERSION))

    graph = nx.DiGraph()

    def add_target(target):
        for node in graph:
            info = graph.nodes[node]["info"]
            if info["name"] == target:
                return node

        c.execute("SELECT path, type, name FROM package WHERE name=?", (target,))
        rows = c.fetchall()

        if len(rows) == 0:
            c.execute("SELECT path, type, name FROM package, package_provides WHERE package.path = package_provides.package AND provides = ?", (target,))
            rows = c.fetchall()

            if len(rows) == 0:
                raise RuntimeError("Unable to find package for %s" % (target,))

        options = [{
            "path": row[0],
            "type": row[1],
            "name": row[2],
        } for row in rows]

        options.sort(key=lambda option: option["type"])

        option = options[0]
        path = option["path"]
        pkgtype = option["type"]
        choice_name = option["name"]

        deptypes = ("all",)
        if pkgtype == "binary":
            deptypes += ("run",)
        elif pkgtype == "source":
            deptypes += ("make",)
        else:
            raise RuntimeError("Unsupported package type %s" % (pkgtype,))

        c.execute("SELECT dependency FROM package_dependency WHERE package=? AND type IN (" + ",".join("?" for _ in deptypes) + ")", (path,) + deptypes)
        dep_rows = c.fetchall()

        needed_deps = query_needed_deps([row[0] for row in dep_rows])

        dep_nodes = []

        for name in needed_deps:
            dep_nodes.append(add_target(name))

        info = {
            "name": choice_name,
            "type": pkgtype
        }

        graph.add_node(path, info=info)
        for dep_node in dep_nodes:
            graph.add_edge(path, dep_node)

        return path

    add_target(root_target)

    processed = set()
    bins_to_install = []
    steps = []

    while True:
        any_remaining = False
        any_changed = False
        for node in graph:
            if node in processed:
                continue
            any_remaining = True

            blocked = False
            for dep in graph.succ[node]:
                if dep not in processed:
                    blocked = True
                    break
            if not blocked:
                any_changed = True
                processed.add(node)
                info = graph.nodes[node]["info"]
                if info["type"] == "binary":
                    bins_to_install.append(node)
                elif info["type"] == "source":
                    if sysroot != "/":
                        pass
                        # raise RuntimeError("Building source packages is not currently supported with the --sysroot option")

                    if bins_to_install:
                        steps.append({"type": "binary", "packages": bins_to_install})
                        bins_to_install = []
                    steps.append({"type": "source", "package": node})
                else:
                    raise RuntimeError("Unrecognized package type: %s" % (info["type"],))
        if not any_remaining:
            break
        if not any_changed:
            raise RuntimeError("Dependency cycle detected.")
    if bins_to_install:
        steps.append({"type": "binary", "packages": bins_to_install})

    items = []
    for step in steps:
        if step["type"] == "binary":
            items += step["packages"]
        elif step["type"] == "source":
            items.append(step["package"] + " [source]")
        else:
            raise RuntimeError("Unrecognized step type: %s" % (step["type"],))

    print("To install:")
    print("\n".join(items))
    print()

    if confirm("Continue?"):
        for step in steps:
            if step["type"] == "binary":
                files = [retrieve_package(package) for package in step["packages"]]

                subprocess.run(["pacman", *pacman_args, "--noconfirm", "-U", *files]).check_returncode()
            elif step["type"] == "source":
                srcfile = retrieve_package(step["package"])

                package = makepkg_from_archive(srcfile, PACKAGE_BUILD_TMP, PACKAGE_BUILD_DEST)
                subprocess.run(["pacman", *pacman_args, "--noconfirm", "-U", package]).check_returncode()
            else:
                raise RuntimeError("Unrecognized step type: %s" % (step["type"]))
finally:
    conn.close()
