import glob
import os
import pathlib
import shutil
import subprocess
import tempfile

BUILD_UID = 752
BUILD_GID = 752

def drop_user_fn(uid, gid):
    def set_ids():
        os.setgid(gid)
        os.setuid(uid)

    return set_ids

def makepkg_from_archive(archive, tmp_dir, dest_dir):
    tmp_dir.mkdir(exist_ok=True, parents=True)
    dest_dir.mkdir(exist_ok=True, parents=True)

    with tempfile.TemporaryDirectory(dir=tmp_dir) as build_dir:
        os.chown(build_dir, BUILD_UID, BUILD_GID)
        subprocess.run(["bsdtar", "-xf", archive], cwd=build_dir, preexec_fn=drop_user_fn(BUILD_UID, BUILD_GID))

        pbg = glob.iglob(build_dir + "/*/PKGBUILD")
        pkgbuild_loc = next(pbg, None)
        if pkgbuild_loc == None:
            raise RuntimeError("Couldn't find PKGBUILD")

        workdir = pathlib.Path(pkgbuild_loc).parent
        print(workdir)

        subprocess.run(["makepkg"], cwd=workdir, preexec_fn=drop_user_fn(BUILD_UID, BUILD_GID)).check_returncode()

        results = glob.glob(str(workdir) + "/*.pkg.tar.*")
        if len(results) < 1:
            raise RuntimeError("No built package found")
        if len(results) > 1:
            raise RuntimeError("More than one built package found")

        return shutil.copy(results[0], dest_dir)
